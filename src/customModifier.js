import { LineAnnotation } from 'scichart/Charting/Visuals/Annotations/LineAnnotation';
import { ChartModifierBase2D } from 'scichart/Charting/ChartModifiers/ChartModifierBase2D';
import { EChart2DModifierType } from 'scichart/types/ChartModifierType';
import { step } from './constants';

export class SimpleChartModifierJs extends ChartModifierBase2D {
    lineAnnotation;
    x;
    y;

    constructor(splineX, splineY) {
        super();
        this.type = EChart2DModifierType.SeriesSelection;
        this.x = [...splineX];
        this.y = [...splineY];

        this.lineAnnotation = new LineAnnotation({
            stroke: '#FF6600',
            strokeThickness: 3,
            x1: 4,
            x2: 8,
            y1: 14,
            y2: 20,
        });
    }
    modifierMouseDown(args) {
        super.modifierMouseMove(args);

        const xAxis = this.parentSurface.xAxes.get(0);
        const yAxis = this.parentSurface.yAxes.get(0);
        const convertXToDataX = xAxis.getCurrentCoordinateCalculator();
        const convertYToDataY = yAxis.getCurrentCoordinateCalculator();
        const dataX = convertXToDataX.getDataValue(args.mousePoint.x);
        const dataY = convertYToDataY.getDataValue(args.mousePoint.y);
        const findXIndex = this.x.findIndex((value) => (value - step / 20) < dataX && (value + step / 10) > dataX);
        if (findXIndex > -1 && dataY < this.y[findXIndex] + 1 && dataY > this.y[findXIndex] - 1) {
            this.parentSurface.annotations.add(this.lineAnnotation);
            const diffY1 = this.y[findXIndex] - this.y[findXIndex - 1];
            const diffY2 = this.y[findXIndex] - this.y[findXIndex + 1];
            let rate = diffY1 / diffY2;
            if ((diffY1 < 0 && diffY2 > 0) || (diffY1 < 0 && diffY2 < 0)) {
                rate *= -1;
            }
            const diff = Math.abs(diffY1 + diffY2);
            this.lineAnnotation.x1 = this.x[findXIndex - 1];
            this.lineAnnotation.y1 = this.y[findXIndex - 1] + diff * rate;
            this.lineAnnotation.x2 = this.x[findXIndex + 1];
            this.lineAnnotation.y2 = this.y[findXIndex + 1] + diff * rate;
        }
    }
    modifierMouseUp(args) {
        super.modifierMouseUp(args);
        this.parentSurface.annotations.remove(this.lineAnnotation);
    }
}