export class Interpolation {
    x;
    y;
    size;

    matrixA;
    matrixB;
    alpha = [];

    constructor(initialX, initialY) {
        this.x = [...initialX];
        this.y = [...initialY];
        this.size = initialX.length;
        this.matrixA = Array(initialX.length).fill('').map(() => Array(initialX.length).fill('').map(() => 0));
        this.matrixB = [...initialY];

        this.generateSolveMatrix();
        this.convertSolveMatrixToTriangleMatrix();
        this.solveLinearFunction();
    }

     generateSolveMatrix() {
        for (let i = 0; i < this.size; i++) {
            for (let j = 0; j < this.size; j++) {
                this.matrixA[i][j] = j === 0 ? 1 : Math.pow(this.x[i], j);
            }
        }
    }

     convertSolveMatrixToTriangleMatrix() {
        for (let i = 0; i < this.size-1; i++) {
            for (let j = i + 1; j < this.size; j++) {
                if (this.matrixA[j][i] === 0) continue;

                const rateA = this.matrixA[j][i] / this.matrixA[i][i];

                for (let z = 0; z < this.size; z++) {
                    this.matrixA[j][z] -= this.matrixA[i][z]*rateA;
                }
                this.matrixB[j] -= this.matrixB[i]*rateA;
            }
        }
    }

     solveLinearFunction() {
        for (let i = this.size -1; i > -1; i--) {
            let sum = 0;

            for (let j =  this.size - 1; j > i; j--) {
                sum += this.matrixA[i][j] * this.alpha[j];
            }

            this.alpha[i] = (this.matrixB[i] - sum) / this.matrixA[i][i];
        }
    }

     getInterpolatedY(x) {
         let sum = 0;
         for (let j = 0; j < this.x.length; j++) {
             sum += this.alpha[j] * Math.pow(x, j);
         }

         return sum;
    }
}