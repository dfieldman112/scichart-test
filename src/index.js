import { SciChartSurface } from 'scichart/Charting/Visuals/SciChartSurface';
import { NumericAxis } from 'scichart/Charting/Visuals/Axis/NumericAxis';
import { FastLineRenderableSeries } from 'scichart/Charting/Visuals/RenderableSeries/FastLineRenderableSeries';
import { XyDataSeries } from 'scichart/Charting/Model/XyDataSeries';
import { XyScatterRenderableSeries } from 'scichart/Charting/Visuals/RenderableSeries/XyScatterRenderableSeries';
import { EllipsePointMarker } from 'scichart/Charting/Visuals/PointMarkers/EllipsePointMarker';
import { Interpolation } from './interpolation.js';
import { SimpleChartModifierJs } from './customModifier';
import { LICENSE, x, y, step } from './constants';

async function initSciChart() {
    const splineX = [];
    const splineY = [];

    SciChartSurface.setRuntimeLicenseKey(LICENSE);

    const {sciChartSurface, wasmContext} = await SciChartSurface.create('scichart-root');
    const xAxis = new NumericAxis(wasmContext);
    const yAxis = new NumericAxis(wasmContext);
    sciChartSurface.xAxes.add(xAxis);
    sciChartSurface.yAxes.add(yAxis);

    const scatterSeries = new XyScatterRenderableSeries(wasmContext, {
        pointMarker: new EllipsePointMarker(wasmContext, {
            width: 7,
            height: 7,
            strokeThickness: 1,
            fill: 'steelblue',
            stroke: 'LightSteelBlue',
        }),
    });
    sciChartSurface.renderableSeries.add(scatterSeries);
    const dataSeries = new XyDataSeries(wasmContext);
    for (let i = 0; i < x.length; i++) {
        dataSeries.append(x[i], y[i]);
    }
    scatterSeries.dataSeries = dataSeries;

    const interpolation = new Interpolation(x, y);

    const xyDataSeries = new XyDataSeries(wasmContext);
    for (let i = x[1]; i < x[x.length - 2]; i += step / 5) {
        const yValue = interpolation.getInterpolatedY(i);
        splineX.push(i);
        splineY.push(yValue)
        xyDataSeries.append(i, yValue);
    }

    const lineSeries = new FastLineRenderableSeries(wasmContext, {
        dataSeries: xyDataSeries,
        stroke: '#ff6600',
        strokeThickness:2
    });
    sciChartSurface.renderableSeries.add(lineSeries);

    sciChartSurface.chartModifiers.add(new SimpleChartModifierJs(splineX, splineY));
}
initSciChart();

