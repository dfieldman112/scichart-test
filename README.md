# derek_scichart_test

1. I made Interpolation function which will return an interpolated Y value for each X value within the xValues interval.
For example, we have below data points.

      x = [1, 2, 3, 4, 5];

      y = [20, 23, 34, 23, 45];

      By using this data, we can render scatter chat.

      In this case, we can't draw splined chart by using only these 5 values.

      So, I made Interpolation function.

      It returns y value between xValues interval (e.g 1 ~ 2, 2 ~ 3);

      Finally, we can get y value according to random x value by using this interpolate function. 


2. I added scatter chart and line chart
![image](https://user-images.githubusercontent.com/97659469/207149091-800540be-1fd3-4ac7-9838-1a69040ff757.png)


3. I added Custom chart modifier.
When user click one point in line chart, it shows custom chart modifier.
![image](https://user-images.githubusercontent.com/97659469/207149138-d902ab83-e4ec-4b68-bc68-ded3181f25b3.png)
